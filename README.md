# aqms-comserv3

## Description

The official repository of Comserv3 locates
    https://github.com/BerkeleySeismoLab/comserv3

Dependencies:
    
  - aqms-comserv2aqms
  - aqms-gcda
  - aqms-libs
  - aqms-multicast

## Authors and acknowledgment
- Doug Neuhauser <dougneuhauser@berkeley.edu>
- Berkeley Seismo Lab, UC Berkeley

